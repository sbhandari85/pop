==============
Pop Release 10
==============

The POP team is pleased to announce the release of pop 10!

Pop-Config
==========

This release makes a major change to how configuration is loaded and deprecates
the ``conf`` system. The ``conf`` system has been pulled out of the ``pop`` code
and placed into the new `pop-config` project. The `pop-config` project is
now a hard dep of `pop`.

A deprecation warning has been added to `conf.integrate` and it will be
removed in an upcoming release of `pop`.

Please see the docs located at https://pop-config.readthedocs.io for more
information about how to use the new config system. This new system still
uses the `conf.py` file but some of the formatting has changed.

Data Structures Improvements
============================

The included data structures continue to improve, this allows us to make
dynamic and immutable data structures a convenience for storing data
on the hub and making the POP experience more native.
